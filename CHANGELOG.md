# Change log

## 0.1.3 (2018-08-12)

### Changes

- Added metadata to gemspec. ([@just3ws][])
- Added CHANGELOG.md. ([@just3ws][])
- Added .editorconfig ([@just3ws][])
