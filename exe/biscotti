#!/usr/bin/env ruby
# frozen_string_literal: true

Signal.trap('INT') { exit(130) }
Signal.trap('SIGINT') { exit(130) }
Signal.trap('TERM') { exit(143) }

require 'bundler/setup'

$LOAD_PATH.unshift("#{__dir__}/../lib")

require 'biscotti'
require 'biscotti/cli'

require 'optparse'

OptionParser.new do |opts|
  opts.banner = 'Usage: bundle exec biscotti [--version] [--help] <letters>'

  opts.on('-V', '--version', 'Print version info') do
    $stdout.puts("biscotti version #{Biscotti::VERSION}")
    exit(0)
  end

  # TODO: Print dictionary
  # TODO: Minimum character count
  # TODO: Provide a custom dictionary
  # TODO: Output as a grouped list by word length
  # TODO: Verbose mode to emit more than just the list
  # TODO: Alternative formats mode: JSON, Yaml, Text (default)

  opts.on_tail('-h', '--help', 'Show this message') do
    warn(opts)
    exit(0)
  end
end.parse!

# raise 'File not processable' unless Biscotti::CLI.processable?

BISCOTTI_HOME = File.realpath(File.join(File.dirname(__FILE__), '..'))

begin
  dictionary = Biscotti.load_dictionary(File.join(BISCOTTI_HOME, 'data', 'biscotti', 'words.lst').freeze)
  letters = ARGV.flat_map { |word| word.downcase.split('') }.sort.join
  min_word_length = 2

  output = Biscotti.find_words(
    letters,
    dictionary: dictionary,
    min_word_length: min_word_length
  )

  output.each do |word|
    $stdout.puts(word)
  end
rescue Errno::EPIPE
  exit(74)
end
